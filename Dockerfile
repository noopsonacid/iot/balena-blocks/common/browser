FROM balenalib/raspberrypi4-64-debian-node:12-buster

# install required packages
RUN install_packages \
    chromium \
    #rpi-chromium-mods \ 
    libgles2-mesa \
    lsb-release \
    mesa-vdpau-drivers \
    wget \
    x11-xserver-utils \
    xserver-xorg-input-evdev \
    xserver-xorg-legacy \
    xserver-xorg-video-fbdev \
    xserver-xorg xinit \
    xterm 

WORKDIR /usr/src/

ARG BALENA_BROWSER_VERSION=2.1.1
RUN wget -cO- "https://github.com/balenablocks/browser/archive/refs/tags/v${BALENA_BROWSER_VERSION}.tar.gz" \
  | tar -xvz -C . && mv browser-* browser

WORKDIR /usr/src/browser

# install node dependencies

RUN JOBS=MAX npm install --unsafe-perm --production && npm cache clean --force

RUN mkdir -p /etc/chromium/policies
RUN mkdir /etc/chromium/policies/managed
RUN mv policy.json /etc/chromium/policies/managed/my_policy.json

RUN chmod +x ./*.sh

ENV UDEV=1

# Add chromium user
RUN useradd chromium -m -s /bin/bash -G root || true && \
    groupadd -r -f chromium && id -u chromium || true \
    && chown -R chromium:chromium /home/chromium || true

RUN mv public-html /home/chromium

# udev rule to set specific permissions 
RUN echo 'SUBSYSTEM=="vchiq",GROUP="video",MODE="0660"' > /etc/udev/rules.d/10-vchiq-permissions.rules
RUN usermod -a -G audio,video,tty chromium

# # Set up the audio block. This won't have any effect if the audio block is not being used.
RUN curl -skL https://raw.githubusercontent.com/balenablocks/audio/master/scripts/alsa-bridge/debian-setup.sh| sh
ENV PULSE_SERVER=tcp:audio:4317

# Start app
CMD ["bash", "start.sh"]